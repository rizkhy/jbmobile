import React, {useEffect} from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {LogoJB} from '../../assets';
import {Button, Gap, TextInput} from '../../components';
import {signInAction} from '../../redux/action/auth';
import {getData, useForm} from '../../utils';

const Login = ({navigation}) => {
  const [form, setForm] = useForm({
    username: '',
    password: '',
  });

  const dispatch = useDispatch();

  const onSubmit = () => {
    dispatch(signInAction(form, navigation));
  };

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.pages}>
        <StatusBar animated={true} backgroundColor="#0092D1" />
        <View style={styles.header}>
          <Image source={LogoJB} style={styles.image} />
          <Text style={styles.text}>Sign In</Text>
        </View>
        <View style={styles.container}>
          <TextInput
            label="Username"
            value={form.username}
            onChangeText={value => setForm('username', value)}
          />
          <Gap height={30} />
          <TextInput
            label="Password"
            value={form.password}
            onChangeText={value => setForm('password', value)}
            secureTextEntry
          />
          <Gap height={60} />
          <Button
            text="Sign In"
            onPress={onSubmit}
            // onPress={() => navigation.navigate('Home')}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({
  scroll: {flexGrow: 1},
  pages: {
    flex: 1,
    backgroundColor: '#0092D1',
  },
  header: {
    minHeight: 230,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    backgroundColor: '#FFFFFF',
    borderRadius: 100,
    height: 110,
    width: 110,
  },
  text: {
    top: 10,
    color: '#FFFFFF',
    fontSize: 28,
    fontFamily: 'BalooBhaijaan-Regular',
  },
  container: {
    flex: 1,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 40,
    paddingVertical: 90,
  },
  formInput: {
    backgroundColor: '#EFEFEF',
    borderRadius: 15,
    width: 300,
    height: 50,
  },
});
