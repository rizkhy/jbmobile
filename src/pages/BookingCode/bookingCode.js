import React from 'react';
import {StatusBar, StyleSheet, Text, View} from 'react-native';
import {Button, Gap, Header, TextInput} from '../../components';
import {showMessage, useForm} from '../../utils';
import {useDispatch} from 'react-redux';
import {getInvoices} from '../../redux/action/invoice';

const BookingCode = ({navigation}) => {
  const [form, setForm] = useForm({
    invoice: '',
  });
  const dispatch = useDispatch();

  const onSubmit = () => {
    console.log(form.invoice);
    dispatch(getInvoices(form.invoice, navigation));
  };

  return (
    <View style={styles.pages}>
      <StatusBar animated={true} backgroundColor="#0092D1" />
      <Header title="Kode Transaksi" onBack={() => navigation.goBack()} />
      <View style={styles.content}></View>
      <Text style={styles.text}>
        Silahkan masukan Kode Transaksi dari E-Ticket pengujung
      </Text>
      <View style={styles.container}>
        <TextInput
          label="Kode Transaksi"
          value={form.invoice}
          onChangeText={value => setForm('invoice', value)}
        />
        <Gap height={60} />
        <Button
          text="Cek"
          onPress={onSubmit}
          // onPress={() => navigation.navigate('Detail')}
        />
      </View>
    </View>
  );
};

export default BookingCode;

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  content: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  text: {
    paddingTop: 50,
    color: '#000',
    fontSize: 20,
    marginHorizontal: 20,
    // fontFamily: 'BalooBhaijaan-Regular',
    textAlign: 'center',
  },
  container: {
    flex: 1,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingHorizontal: 45,
    paddingVertical: 90,
  },
});
