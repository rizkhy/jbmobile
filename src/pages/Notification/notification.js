import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {LogoJB, Success} from '../../assets';
import {Button, Gap} from '../../components';

const Notification = ({navigation}) => {
  const {invoice} = useSelector(state => state.invoiceReducer);
  console.log(invoice);

  return (
    <View style={styles.pages}>
      <StatusBar animated={true} backgroundColor="#0092D1" />
      <Image source={LogoJB} style={styles.logo} />
      <View style={styles.content}>
        <Image source={Success} style={styles.image} />
        <Gap height={35} />
        <Text
          style={{
            fontSize: 24,
            fontFamily: 'BalooBhaijaan-Regular',
            color: '#000',
          }}>
          Ahooy!!
        </Text>
        <Gap height={9} />
        <Text style={{fontSize: 18, color: '#7D8797'}}>
          Data pengunjung berhasil dikonfirmasi
        </Text>
        <Text
          style={{
            textAlign: 'center',
            paddingHorizontal: 10,
            color: '#7D8797',
          }}>
          Silahkan kembali ke Home untuk mengkonfirmasi pengunjung lainnya
        </Text>
        <Gap height={50} />
        <Button text="Kembali" onPress={() => navigation.navigate('Home')} />
      </View>
    </View>
  );
};

export default Notification;

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: '#fff',
  },
  logo: {
    height: 84,
    width: 92,
    marginLeft: 10,
    marginTop: 10,
  },
  content: {
    flex: 1,
    // backgroundColor: '#000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 250,
    width: 181,
  },
});
