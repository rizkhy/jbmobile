import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const ItemValue = ({label, value, valueColor = '#7D8797', type}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{label}</Text>
      {type === 'Belum Datang' ? (
        <Text style={styles.notyet}>{value}</Text>
      ) : type === 'Sudah Datang' ? (
        <Text style={styles.arrival}>{value}</Text>
      ) : (
        <Text style={styles.value(valueColor)}>{value}</Text>
      )}
    </View>
  );
};

export default ItemValue;

const styles = StyleSheet.create({
  container: {flexDirection: 'row', justifyContent: 'space-between'},
  label: {fontSize: 14, color: '#000'},
  value: color => ({
    fontSize: 14,
    color: color,
  }),
  notyet: {
    fontSize: 14,
    color: '#FFFFFF',
    backgroundColor: '#CD2601',
    padding: 5,
    borderRadius: 5,
  },
  arrival: {
    fontSize: 14,
    color: '#FFFFFF',
    backgroundColor: '#00B934',
    padding: 5,
    borderRadius: 5,
  },
});
