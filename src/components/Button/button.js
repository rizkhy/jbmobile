import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const Button = ({
  text,
  color = '#0092D1',
  textColor = '#FFFFFF',
  width = 300,
  onPress,
}) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <View style={styles.container(color, width)}>
        <Text style={styles.text(textColor)}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: (color, width) => ({
    backgroundColor: color,
    padding: 10,
    borderRadius: 10,
    width: width,
  }),
  text: color => ({
    fontSize: 20,
    fontFamily: 'BalooBhaijaan-Regular',
    color: color,
    textAlign: 'center',
  }),
});
