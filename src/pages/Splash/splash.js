import React, {useEffect} from 'react';
import {View, Text, Image, StyleSheet, StatusBar} from 'react-native';
import {LogoJB} from '../../assets';
import {getData} from '../../utils';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      getData('token').then(res => {
        console.log('token: ', res);
        if (res) {
          navigation.reset({index: 0, routes: [{name: 'Home'}]});
        } else {
          navigation.replace('Login');
        }
      });
    }, 2000);
  }, []);

  return (
    <View style={styles.page}>
      <StatusBar
        hidden={true}
        translucent={false}
        animated={true}
        backgroundColor="#FFF"
      />
      <Image source={LogoJB} style={styles.image} />
      <Text style={styles.text}>Jogja Bay App</Text>
      <Text style={styles.text}>Ticketing</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#FFFFFF',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 149,
    width: 162,
  },
  text: {
    color: '#0092D1',
    fontSize: 21,
    fontFamily: 'BalooBhaijaan-Regular',
  },
});
