import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import {LogoJB} from '../../assets';
import {Button, Gap, Header, ItemValue} from '../../components';
import {useDispatch, useSelector} from 'react-redux';
import {changeArrivalStatus} from '../../redux/action';

const Detail = ({navigation}) => {
  const dispatch = useDispatch();

  // const dispatch = useDispatch();
  const {invoice} = useSelector(state => state.invoiceReducer);
  // console.log(invoice);

  const onConfirm = () => {
    dispatch(changeArrivalStatus(invoice.transaction_code, navigation));
  };

  return (
    <View style={styles.pages}>
      <StatusBar animated={true} backgroundColor="#0092D1" />
      <Header title="Detail Pemesanan" onBack={() => navigation.goBack()} />
      <View style={styles.transaction}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Image source={LogoJB} style={styles.image} />
          <View style={{justifyContent: 'center'}}>
            <Text
              style={{textAlign: 'right', color: '#7D8797', marginBottom: 5}}>
              Tanggal Pesan
            </Text>
            <Text style={{textAlign: 'right', color: '#000'}}>
              {invoice.created_at}
            </Text>
          </View>
        </View>
        <Gap height={20} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={{justifyContent: 'center'}}>
            <Text style={{textAlign: 'left', color: '#000'}}>
              Kode Transaksi
            </Text>
            <Text
              style={{
                textAlign: 'left',
                color: '#E83800',
                fontSize: 18,
                fontWeight: '900',
              }}>
              {invoice.transaction_code}
            </Text>
          </View>
          <Gap height={15} />
          <View style={{justifyContent: 'center'}}>
            <Text style={{textAlign: 'right', color: '#000'}}>
              Kode Booking
            </Text>
            <Text
              style={{
                textAlign: 'right',
                color: '#E83800',
                fontSize: 18,
                fontWeight: '900',
              }}>
              {invoice.booking_code}
            </Text>
          </View>
        </View>
        <Gap height={15} />
        <ItemValue label="Nama" value={invoice.name} />
        <Gap height={18} />
        <ItemValue label="Alamat" value={invoice.city} />
        <Gap height={18} />
        <ItemValue label="Telepon" value={invoice.phone_number} />
        <Gap height={18} />
        <ItemValue label="E-Mail" value={invoice.email} />
        <Gap height={18} />
        <ItemValue
          label="Status"
          value={invoice.arrival_status}
          type={invoice.arrival_status}
        />
      </View>
      <View style={styles.order}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={{justifyContent: 'center'}}>
            <Text
              style={{textAlign: 'left', color: '#7D8797', marginBottom: 5}}>
              Tanggal Datang
            </Text>
            <Text style={{textAlign: 'left', color: '#000'}}>
              {invoice.arrival_date}
            </Text>
          </View>
          <View style={{justifyContent: 'center'}}>
            <Text
              style={{textAlign: 'left', color: '#7D8797', marginBottom: 5}}>
              Dewasa
            </Text>
            <Text style={{textAlign: 'left', color: '#000'}}>
              {invoice.adults} Orang
            </Text>
          </View>
          <View style={{justifyContent: 'center'}}>
            <Text
              style={{textAlign: 'left', color: '#7D8797', marginBottom: 5}}>
              Anak
            </Text>
            <Text style={{textAlign: 'left', color: '#000'}}>
              {invoice.kids} Orang
            </Text>
          </View>
        </View>
      </View>
      <Text
        style={{
          color: '#FFF',
          textAlign: 'center',
          paddingHorizontal: 40,
          marginVertical: 17,
        }}>
        Anda ingin mengkonfirmasi kedatangan pengunjung ini ?
      </Text>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Button text="Konfirmasi" color="#00B934" onPress={onConfirm} />
      </View>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: '#007EBD',
  },
  transaction: {
    // flex: 1,
    backgroundColor: '#FFF',
    marginHorizontal: 15,
    marginTop: 20,
    borderRadius: 10,
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  image: {
    height: 60,
    width: 60,
  },
  order: {
    // flex: 0,
    backgroundColor: '#FFF',
    marginHorizontal: 15,
    marginTop: 1,
    borderRadius: 10,
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
});
