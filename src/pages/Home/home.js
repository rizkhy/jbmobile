import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {LogoJB, QrCode, Ticket} from '../../assets';
import {Button, Gap} from '../../components';

const Home = ({navigation}) => {
  const signOut = () => {
    AsyncStorage.removeItem('token').then(() => {
      navigation.reset({index: 0, routes: [{name: 'Login'}]});
    });
  };

  return (
    <View style={styles.page}>
      <StatusBar animated={true} backgroundColor="#0092D1" />
      <Image source={LogoJB} style={styles.image} />
      <Text
        style={{
          color: '#000',
          fontSize: 24,
          fontFamily: 'BalooBhaijaan-Regular',
        }}>
        Ahooy!!
      </Text>
      <Text
        style={{
          color: '#7D8797',
          fontSize: 18,
        }}>
        Selamat Datang Petugas
      </Text>
      <View style={styles.menu}>
        <TouchableOpacity
          style={styles.qrcode}
          activeOpacity={0.7}
          onPress={() => navigation.navigate('Scan')}>
          <QrCode width={67} height={85} />
          <Text style={styles.text}>QRCODE</Text>
        </TouchableOpacity>
        <Gap width={15} />
        <TouchableOpacity
          style={styles.bookingCode}
          activeOpacity={0.7}
          onPress={() => navigation.navigate('BookingCode')}>
          <Ticket width={100} />
          <Text style={styles.text}>KODE TRANSAKSI</Text>
        </TouchableOpacity>
      </View>
      {/* <TouchableOpacity
        style={styles.qrcode}
        activeOpacity={0.7}
        onPress={() => navigation.navigate('Scan')}>
        <QrCode />
        <Text style={styles.text}>QRCODE</Text>
      </TouchableOpacity>
      <Gap height={3} />
      <TouchableOpacity
        style={styles.bookingCode}
        activeOpacity={0.7}
        onPress={() => navigation.navigate('BookingCode')}>
        <Ticket />
        <Text style={styles.text}>KODE TRANSAKSI</Text>
      </TouchableOpacity> */}
      <Button text={'Keluar'} color={'#A30404'} width={200} onPress={signOut} />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menu: {
    // flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 15,
    marginTop: 90,
    marginBottom: 51,
  },
  qrcode: {
    flex: 1,
    backgroundColor: '#0092D1',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 15,
  },
  bookingCode: {
    flex: 1,
    backgroundColor: '#0092D1',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 15,
  },
  text: {
    // paddingTop: 16,
    color: '#fff',
    fontFamily: 'BalooBhaijaan-Regular',
    fontSize: 18,
  },
  image: {
    height: 200,
    width: 200,
    marginBottom: 21,
  },
});
