import BookingCode from './BookingCode/bookingCode';
import Detail from './Detail/detail';
import Home from './Home/home';
import Login from './Login/login';
import Notification from './Notification/notification';
import Scan from './Scan/scan';
import Splash from './Splash/splash';

export {Splash, Login, Home, Scan, Detail, BookingCode, Notification};
