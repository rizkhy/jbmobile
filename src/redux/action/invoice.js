import Axios from 'axios';
import {setLoading} from '.';
import {URL_GET_INVOICE, URL_POST_CHANGE_STATUS} from '../../config';
import {getData, showMessage} from '../../utils';

export const getInvoices = (bookingCode, navigation) => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    // console.log(resToken);
    Axios.get(`${URL_GET_INVOICE}${bookingCode}`, {
      headers: {
        Authorization: `${resToken.value}`,
      },
    })
      // console.log(resToken.value);
      .then(res => {
        // console.log(resToken.value);
        // console.log(res.data.data);
        dispatch(setLoading(false));
        dispatch({type: 'SET_INVOICE', value: res.data.data});
        showMessage('Scan Qr Code Berhasil', 'success');
        navigation.navigate('Detail');
      })
      .catch(err => {
        console.log(resToken.value);
        console.log(err.response);
        dispatch(setLoading(false));
        showMessage(`${err?.response?.data?.message}`);
      });
  });
};

export const changeArrivalStatus = (bookingCode, navigation) => dispatch => {
  dispatch(setLoading(true));
  getData('token').then(resToken => {
    console.log('token : ', resToken);
    console.log('url : ', URL_POST_CHANGE_STATUS + bookingCode);
    Axios.post(`${URL_POST_CHANGE_STATUS}${bookingCode}`, bookingCode, {
      headers: {
        Authorization: `${resToken.value}`,
      },
    })
      // console.log(resToken.value);
      .then(res => {
        // console.log(resToken.value);
        dispatch(setLoading(false));
        console.log('sukses :', res.data.data);
        dispatch({type: 'SET_STATUS', value: res.data.data});
        navigation.navigate('Notification');
      })
      .catch(err => {
        // console.log(resToken.value);
        dispatch(setLoading(false));
        console.log('error : ', err.response);
        showMessage(`${err?.response?.data?.message}`);
      });
  });
};
