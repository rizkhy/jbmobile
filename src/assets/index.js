import LogoJB from './icons/jogjabay.png';
import IcBack from './icons/ic-back-white.svg';
import Ticket from './icons/ticket.svg';
import QrCode from './icons/qrcode.svg';
import Success from './icons/success.png';
import Failed from './icons/failed.png';

export {LogoJB, IcBack, Ticket, QrCode, Success, Failed};
