const initInvoice = {
  invoice: [],
  status: [],
};

export const invoiceReducer = (state = initInvoice, action) => {
  if (action.type === 'SET_INVOICE') {
    return {
      ...state,
      invoice: action.value,
    };
  }
  if (action.type === 'SET_STATUS') {
    return {
      ...state,
      status: action.value,
    };
  }
  return state;
};
