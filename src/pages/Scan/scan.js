'use strict';
import React, {PureComponent} from 'react';
import {
  LogBox,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import BarcodeMask from 'react-native-barcode-mask';
import {RNCamera} from 'react-native-camera';
import {Button, Header} from '../../components';
import {useDispatch} from 'react-redux';
import {getInvoices} from '../../redux/action/invoice';
import store from '../../redux/store';
import {setLoading} from '../../redux/action';
import {showMessage} from '../../utils';

LogBox.ignoreLogs(['Remote debugger']);

class Scan extends PureComponent {
  state = {
    barcode: '',
  };
  navigation = this.props;
  // dispatch = useDispatch();

  onSubmit = () => {
    console.log(this.barcode);
    dispatch(getInvoices(this.barcode, this.props.navigation));
  };

  render() {
    // console.log(this.barcode);
    return (
      <View style={styles.container}>
        <StatusBar animated={true} backgroundColor="#0092D1" />
        <Header
          title="Scan Qr Code"
          onBack={() => this.props.navigation.goBack()}
        />
        <View style={{flex: 1}}>
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={styles.preview}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.on}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            onBarCodeRead={barcode => {
              console.log(barcode.data.substring(0, 2));
              this.setState({barcode: barcode.data});
              if (barcode.data.substring(0, 2) == 'JB') {
                // dispatch(getInvoices(barcode.data, this.props.navigation));
                store.dispatch(setLoading(true));
                store.dispatch(
                  getInvoices(barcode.data, this.props.navigation),
                );
                // this.props.navigation.navigate('Detail');
              } else {
                store.dispatch(showMessage('Data tidak ditemukan', 'error'));
              }
            }}>
            <BarcodeMask
              width={280}
              height={280}
              edgeColor={'#62B1F6'}
              edgeBorderWidth={3}
              transparency={0.7}
            />
          </RNCamera>
        </View>
        {/* <View style={{paddingTop: 40, alignItems: 'center', paddingBottom: 20}}>
          <Button text={'Scan'} />
          <Text style={{color: '#000'}}>{`Hasil ${this.state.barcode}`}</Text>
        </View> */}
      </View>
    );
  }
}

export default Scan;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'black',
  },
  preview: {
    width: '100%',
    height: '100%',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    position: 'absolute',
  },
});
