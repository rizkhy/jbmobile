import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {IcBack} from '../../assets';

const Header = ({title, onBack}) => {
  return (
    <View style={styles.container}>
      {onBack && (
        <TouchableOpacity activeOpacity={0.7} onPress={onBack}>
          <View style={styles.back}>
            <IcBack style={{color: '#FFFFFF'}} />
          </View>
        </TouchableOpacity>
      )}
      <View>
        <Text style={styles.title}>{title}</Text>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#0092D1',
    // justifyContent: 'space-between',
  },
  title: {fontSize: 22, color: '#FFFFFF'},
  subTitle: {fontSize: 14, color: '#8D92A3'},
  back: {
    padding: 20,
    marginRight: 20,
    marginLeft: -10,
    // backgroundColor: 'red',
  },
});
