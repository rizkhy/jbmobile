export const BASE_URL = 'https://jogjabay.com';

const BASE_API_URL = `${BASE_URL}/api`;

export const URL_POST_LOGIN = `${BASE_API_URL}/login`;
export const URL_GET_INVOICE = `${BASE_API_URL}/invoice/`;
export const URL_POST_CHANGE_STATUS = `${BASE_API_URL}/change-invoice/`;
