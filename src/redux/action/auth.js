import Axios from 'axios';
import {URL_POST_LOGIN} from '../../config';
import {showMessage, storeData} from '../../utils';
import {setLoading} from './global';

export const signInAction = (form, navigation) => dispatch => {
  dispatch(setLoading(true));
  // console.log(form);
  Axios.post(`${URL_POST_LOGIN}`, form)
    .then(res => {
      const token = `${res.data.data.token_type} ${res.data.data.access_token}`;
      console.log(res);
      // const profile = res.data.data.user;
      dispatch(setLoading(false));
      storeData('token', {value: token});
      showMessage('Login Berhasil', 'success');
      // storeData('userProfile', profile);
      navigation.reset({index: 0, routes: [{name: 'Home'}]});
    })
    .catch(err => {
      dispatch(setLoading(false));
      showMessage(err?.response?.data?.data?.message);
    });
};
